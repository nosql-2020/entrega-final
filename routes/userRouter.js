var express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

const cache = require('../middleware/redisCache');

const userController = require('../controllers/userController');
const commentController = require('../controllers/commentController')

router.use(bodyParser.json());

/*
* Path:  /users
*/
router.route('/')
.get( async (req, res, next) => {
  return await userController.getUsers(req,  res, next).catch((err) => {
    if (err) console.error(err)
  });
})
.post( async (req, res, next) => {
  return await userController.addUser(req,  res, next).catch((err) => {
    if (err) console.error(err)
  });
});


/*
* Path:  /users/:userEmail
*/
router.route('/:userEmail')
.get( async (req, res, next) => {
  await userController.getUserByEmail(req, res, next).catch((err) => {
    if (err) console.error(err)
  });
});


/*
* Path:  /users/:userEmail/comments
* Middleware: search in cache
*/
router.route('/:userEmail/comments')
.get( async (req, res, next) => {
  var comments = await cache.getUserCommentsByEmail(req.params.userEmail).catch((err) => {
    if (err) console.error(err)
  }); 
  if(comments){
    console.log('middleware: esta  en cache');
    res.json(comments);
  } else {
    console.log('middleware: No  esta  en cache');
    next();
  }
});

/*
* Path:  /users/:userEmail/comments
*/
router.route('/:userEmail/comments')
.get( async (req, res, next) => {
  await commentController.getCommentsByEmail(req, res, next).catch((err) => {
    if (err) console.error(err)
  });
})
.post( async (req, res, next) => {
  await commentController.addComment(req, res, next).catch((err) => {
    if (err) console.error(err)
  });
});

module.exports = router;