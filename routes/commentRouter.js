var express = require('express');
const bodyParser = require('body-parser');
var router = express.Router();

const commentController = require('../controllers/commentController')

router.use(bodyParser.json());

/*
* Path:  /comments/:commentId
*/
router.route('/:commentId')
.get( async (req, res, next) => {
  return await commentController.getCommentById(req, res, next).catch((err) => {
    if (err) console.error(err)
  });
});


/*
* Path:  /comments/:commentId/reactions
*/
router.route('/:commentId/reactions')
.post( async (req, res, next) => {
  return commentController.addReactionToComment(req, res, next).catch((err) => {
    if (err) console.error(err)
  });
});

module.exports = router;