const client = require('redis').createClient();
const { promisify } = require("util");


client.on('connect', () => {
    console.log('connected to Redis');
});
client.on('error', (err) => {
    console.log("Error " + err);
});


exports.zrange = promisify(client.zrange).bind(client);

exports.zincrby  = promisify(client.zincrby).bind(client);

exports.zrank  = promisify(client.zrank).bind(client);

exports.zremrangebyscore  = promisify(client.zremrangebyscore).bind(client);

exports.zadd  = promisify(client.zadd).bind(client);