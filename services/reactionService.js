const mongoose = require('mongoose');
const Reaction = require('../models/reactions');

exports.createReaction = async (user, like) => {
    var reaction = new Reaction({
        like: like,
        user: user.email
    })
    return Reaction.create(reaction);
};