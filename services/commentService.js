const mongoose = require('mongoose');
const Comments = require('../models/comments');

exports.createComment = async (user, newComment) => {
    var comment = new Comments({
        text: newComment.text,
        user: user._id
    })

    return Comments.create(comment);
};

exports.getCommentById  = async (id) => {
    return Comments.findById(id);
};

exports.addReactionToComment = async (comment, reaction) => {
    comment.reactions.push(reaction);
    return comment.save();
};