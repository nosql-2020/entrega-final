const mongoose = require('mongoose');

const Users = require('../models/users');

exports.getUsers = async () => {
    return Users.find({}) 
};

exports.addUser = async (newUser) => {
    return Users.create(newUser);
};

exports.getUserById  = async (id) => {
    return Users.findById(id);
};

exports.getUserByEmail = async (email) => {
    return Users.findOne({ 'email': email });
};

exports.getUserByComment = async (comment) => {
    return Users.findOne({ "comments._id": comment._id});
};
 
exports.addComment = async (user, newComment) => {
    user.comments.push(newComment);
    return user.save()  
};