const redisService = require('../services/redisService');

const lastVisitedUsersKey = "users:lastVisited";
const userWindowSize = 4;

exports.getUserCommentsByEmail = async (email) => {
        var result = await redisService.zrange(lastVisitedUsersKey, 0, userWindowSize-1, 'withscores').catch((err) => {
            if (err) console.error(err)
        });
        
        var users = [];
        var searchedUser = null;
        var searchedUserIndex = null;

        result.forEach((item, index) => {
            if(index % 2 == 0){
                var user = JSON.parse(item);
                users[Math.floor(index/2)] = { user : user, score: null};
                if(user.email == email){
                    searchedUser = user;
                }
            } else {
                users[Math.floor(index/2)].score = item;
                if(searchedUser && !searchedUserIndex){
                    searchedUserIndex = Number(item);
                }
            }
        });
        
        if(searchedUser && searchedUserIndex > 0){
            await redisService.zincrby(lastVisitedUsersKey, -searchedUserIndex ,JSON.stringify(searchedUser)).catch((err) => {
                if (err) console.error(err)
            });
            for(var i = 0 ;  i < searchedUserIndex; i++){
                await redisService.zincrby(lastVisitedUsersKey, 1 ,JSON.stringify(users[i].user)).catch((err) => {
                    if (err) console.error(err)
                });
            }
        }        
        return searchedUser.comments; 
};

exports.addUserComments = async (user) => {

    var redisUser = JSON.stringify({ email: user.email, comments: user.comments});
    
    var searchedUser = await redisService.zrank(lastVisitedUsersKey, redisUser).catch((err) => {
        if (err) console.error(err)
    });

    if(!searchedUser){
        await redisService.zremrangebyscore(lastVisitedUsersKey, userWindowSize, 9999999).catch((err) => {
            if (err) console.error(err)
        });

        var result =  await redisService.zrange(lastVisitedUsersKey, 0, userWindowSize-1).catch((err) => {
            if (err) console.error(err)
        });

        var users = [];
        result.forEach( async (item) => {
            await redisService.zincrby(lastVisitedUsersKey, 1 ,item).catch((err) => {
                if (err) console.error(err)
            });            
        });

        await redisService.zadd(lastVisitedUsersKey, 0, redisUser).catch((err) => {
            if (err) console.error(err)
        });
    }

};