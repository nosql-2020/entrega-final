const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reactionSchema = require("./reactions").schema;

var commentSchema = new Schema(
  {
    text: {
      type: String,
      required: true,
      maxlength: 256,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: false,
    },
    reactions: [reactionSchema],
  },
  {
    timestamps: true,
  }
);

var Comments = mongoose.model('Comment', commentSchema);

module.exports = Comments;
