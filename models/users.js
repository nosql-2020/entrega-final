const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Comment = require("./comments");

var userSchema = new Schema(
{
  name: {
    type: String,
    required: true,
  },
  email: { 
      type: String, 
      required: true,
    },
    email: { 
        type: String, 
        required: true,
        unique: true 
    }, 
    comments: [Comment.schema]

}, {
    timestamps: true
});

var Users = mongoose.model("User", userSchema);

module.exports = Users;
