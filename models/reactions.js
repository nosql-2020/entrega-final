const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var reactionSchema = new Schema({
    like: {
      type: Boolean,
      required: true,
    },
    user: {
      type: String
    },
  },
  {
    timestamps: true,
  }
);

var Reactions = mongoose.model("Reaction", reactionSchema);

module.exports = Reactions;
