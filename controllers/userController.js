var UserService = require('../services/userService');

exports.getUsers = async (req, res, next) => {
    var users = await UserService.getUsers().catch((err) => {
        if (err) console.error(err)
    }); 
    res.statusCode  = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(users); 
};

exports.addUser = async (req, res, next) => {
    var user = await UserService.addUser(req.body).catch((err) => {
        if (err) console.error(err)
    }); 
    res.statusCode  = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(user);  
};

exports.getUserById = async (req, res, next) => {
    var user = await UserService.getUserById(req.params.userId).catch((err) => {
        if (err) console.error(err)
    }); 
    res.statusCode  = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(user);   
};

exports.getUserByEmail = async (req, res, next) => {
    var user = await UserService.getUserByEmail(req.params.userEmail).catch((err) => {
        if (err) console.error(err)
    }); 
    res.statusCode  = 200;
    res.setHeader('Content-Type', 'application/json');
    res.json(user);   
};