var UserService = require('../services/userService');
var CommentService = require('../services/commentService');
var ReactionService = require('../services/reactionService');
const cache = require('../middleware/redisCache');

exports.getCommentsByEmail = async (req, res, next) => {
    var user = await UserService.getUserByEmail(req.params.userEmail).catch((err) => {
        if (err) next(err)
    });
  
    if(user){
        await cache.addUserComments(user).catch((err) => {
            if (err) console.error(err)
        }); 
        res.statusCode  = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(user.comments);
    } else {
        err = new Error('User not found');
        err.status = 404;
        return next(err);
    }  
};

exports.addComment = async (req, res, next) => {
    var user = await UserService.getUserByEmail(req.params.userEmail).catch((err) => {
        if (err) next(err)
    });
    if(user){
        var comment = await CommentService.createComment(user, req.body).catch((err) => {
            if (err) next(err)
        });
        if(comment){
            user = await UserService.addComment(user, comment).catch((err) => {
                if (err) next(err)
            });
            res.statusCode  = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(user.comments);
        }    
    } else {
        err = new Error('User not found');
        err.status = 404;
        return next(err);
    }
};

exports.addReactionToComment = async (req, res, next) => {
    var comment = await CommentService.getCommentById(req.params.commentId).catch((err) => {
        if (err) next(err)
    });
    if(comment) {
        //Obtengo el usuario que va a reaccionar
        var user = await UserService.getUserByEmail(req.body.email).catch((err) => {
            if (err) next(err)
        });
        if(user){
            var reaction = await ReactionService.createReaction(user, req.body.like).catch((err) => {
                if (err) next(err)
            });
            //Obtengo el usuario creador del comentario
            var commentUser = await UserService.getUserById(comment.user).catch((err) => {
                if (err) next(err)
            });

            var comment = await CommentService.addReactionToComment(comment, reaction).catch((err) => {
                if (err) next(err)
            });
            res.statusCode  = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(comment);     
            
        } else {
            err = new Error('User not found');
            err.status = 404;
            return next(err);
        }
              
    } else {
        err = new Error('Comment not found');
        err.status = 404;
        return next(err);
    }
};

exports.getCommentById = async (req, res, next) => {
    var comment = await CommentService.getCommentById(req.params.commentId).catch((err) => {
        if (err) next(err)
    });
    if(comment) {
        console.log("CANTIDAD DE REACCIONES: " + comment.reactions.length)
        var meGusta = 0;
        var noMeGusta = 0;
        comment.reactions.forEach(function(reaction) {
            if (reaction.like) {
                meGusta++;
            } else {
                noMeGusta++;
            }
        });

        var data = {
            id: comment._id,
            text: comment.text,
            usuario_id: comment.user,
            me_gusta: meGusta,
            no_me_gusta: noMeGusta
        };

        res.statusCode  = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(data);
    } else {
        err = new Error('Comment not found');
        err.status = 404;
        return next(err);
    } 
    
};