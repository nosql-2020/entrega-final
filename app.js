var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');



/*
* Configuracion de Rutas 
*/
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/userRouter');
var commentsRouter = require('./routes/commentRouter');

/*
* Configuracion de Mongo
*/
const url = 'mongodb://localhost:27017/';
const dbName = 'nosqlTwitter';
const connect = mongoose.connect(url + dbName, {useNewUrlParser: true});

connect.then((db) => {
  console.log("connected to Mongo");
}, (err) => {console.log(err);});

/*
* Configuracion de Express 
*/
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
//app.use('/comments', c);
app.use('/comments', commentsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
