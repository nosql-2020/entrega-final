# Obligatorio de NoSQL - 2020

## Integrantes:

-   Ramiro Alves
-   Martin Navarrete
-   Mateo Sayas

## Requisitos:

-   NodeJS
-   NPM
-   MongoDB 
-   Redis


## Pasos a seguir:

1.  Correr en consola en el directorio  del proyecto ``` npm install ```

2.  Crear una base de datos en mongoDB con nombre 'nosqlTwitter', 
    en una consola escribir ``` mongo ``` y luego en el MongoDB Shell
    escribir ``` use nosqlTwitter ```  y ``` db.test.insert({ name :  "test"}); ```. 
    _Requiere MongoDB Shell_.

3.  Correr el proyecto con ``` npm start ```

## Documentacion y coleccion de postman

La documentacion y la coleccion de postman se encuentran en el directorio /docs

